import os
import numpy as np
import cv2

'''
This script is for loading the model_2 combined scores file which has 2 classes
and subtracting the overlap truth for calculating metrics and rendering
'''
		
def parseArgs():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("inputScoreFile")
	parser.add_argument("outputScoreFile")
	parser.add_argument("truthDir")
	args = parser.parse_args()
			
	return args
	
if __name__ == "__main__":
	args = parseArgs()
	
	scores = np.load(args.inputScoreFile)
		
	outDict = {}
	for k in scores.keys():
		truthImage = cv2.imread('{}/{}'.format(args.truthDir, k))[:,:,::-1]
		overlap = np.all(truthImage==(0, 255, 0), axis=-1)
		scoreA = np.minimum(scores[k][0], overlap==0)
		scoreV = np.minimum(scores[k][1], overlap==0)
		empty = np.zeros_like(scoreA)
		outDict[k] = np.stack([empty, scoreA, scoreV, empty], axis=0)
		
	np.savez(args.outputScoreFile, **outDict)