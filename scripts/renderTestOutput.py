import os
import numpy as np
import cv2
from matplotlib import pyplot as plt
import DRIVE_common as DRIVE

# TODO: refactor so that classes mappings between scores and truths are parameterized and general

'''
This script is for loading and plotting/rendering model scores, along with ground truths, to files. The test scores file is a numpy file - a dictionary with keys as DRIVE filenames (e.g. 01_test.png) and values as the respective ndarray output from the Caffe model.
The ground truth images (1-20) must all exist in scoreFilePath and follow DRIVE file naming convention.

TODO: Generalize for any image dataset:
	Compose DRIVE (or other dataset) constants into a property file
	Take command argument for property file and load into prototype object
	Refactor code to reference constants from prototype object
	Generalize/externalize the file naming convention
'''
class TestRenderer():
	def __init__(self, truthDir, scoreFile, outputDir):
		self.truthDir = truthDir
		self.scoreFile = scoreFile
		self.checkLoadInputs()
		self.outputDir = outputDir
		self.checkCreateOutDirs()		

	def checkLoadInputs(self):	
		if not os.path.exists(self.truthDir):
			raise IOError("Truth images directory does not exist: {}".format(self.truthDir))
		if not os.path.exists(self.scoreFile):
			raise IOError("Score file does not exist: {}".format(self.scoreFile))	
		self.scoreDict = np.load(self.scoreFile)
		
	def checkCreateOutDirs(self):
		if not os.path.exists(self.outputDir):
			os.makedirs(self.outputDir)
		self.scoreMapsDir = '{}/{}'.format(self.outputDir, 'scoreMaps')
		if not os.path.exists(self.scoreMapsDir):
			os.makedirs(self.scoreMapsDir)
		self.predictionsDir = '{}/{}'.format(self.outputDir, 'predictions')
		if not os.path.exists(self.predictionsDir):
			os.makedirs(self.predictionsDir)	
			
	def render(self):
		for imageName, inputData in self.scoreDict.iteritems():			
			truthImageFilePath = '{}/{}'.format(self.truthDir, imageName)
			if not os.path.exists(truthImageFilePath):
				raise IOError("Truth image does not exist: {}".format(truthImageFilePath))	
			truthImage = cv2.imread(truthImageFilePath)
			scores = self.scoreDict[imageName]
			print 'Rendering image: {}'.format(imageName)
			self.renderScores(truthImage, scores, imageName)
			self.renderPredictions(truthImage, scores, imageName)
			
	def renderScores(self, truthImage, scores, imageName, dpi=72.):
		n = scores.shape[0] # number of classes (2 for artery/vein only, 4 includes background and overlap)
		plotWidth = 1.0 / n
		height = truthImage.shape[0]
		width = truthImage.shape[1]
		figure = plt.figure(figsize=(n*width/dpi,2*height/dpi))
		truths = DRIVE.truthImageToLabelImage(n == 2, truthImage)
		for i in range(n):
			c = i if n == 4 else i+1
			ax1 = figure.add_axes([i*plotWidth, 0, plotWidth, 0.5], frameon=True, xticks=[],yticks=[])
			ax1.imshow(scores[i], cmap=DRIVE.classCMaps[c], interpolation='none')
			ax2 = figure.add_axes([i*plotWidth, .5, plotWidth, 0.5], frameon=True, xticks=[],yticks=[])
			ax2.imshow(truths[i], cmap=DRIVE.classCMaps[c], interpolation='none')
		figure.savefig('{}/{}'.format(self.scoreMapsDir, imageName), dpi=dpi)
		plt.close(figure)
		
	def predictionToRGB(self, p):
		return np.stack([p == 1, p == 3, p == 2], 2).astype(np.uint8) * 255

	def renderPredictions(self, truthImage, scores, imageName, dpi = 72.):
		
		# assume 2 classes means artery, vein only
		if scores.shape[0] == 2: 
			# create an empty background
			background = np.zeros_like(scores[0])
			scores = np.stack([background, scores[0], scores[1]])
			
		height = truthImage.shape[0]
		width = truthImage.shape[1]
		truthBinary = (np.all(truthImage==DRIVE.classRGBs[0], axis=-1)+\
						np.all(truthImage==DRIVE.unknownRGB, axis=-1))==0
		predictionBG = self.predictionToRGB(np.argmax(scores, axis=0))
		predictionNoBG = self.predictionToRGB(np.multiply(np.argmax(scores[1:], 0)+1, truthBinary))	
		figure = plt.figure(figsize=(3*width/dpi,height/dpi))
		ax1 = figure.add_axes([0, 0, 1./3, 1], frameon=True, xticks=[], yticks=[])
		ax1.imshow(truthImage, interpolation='none')
		ax2 = figure.add_axes([1./3, 0, 1./3, 1], frameon=True, xticks=[], yticks=[])
		ax2.imshow(predictionBG, interpolation='none')
		ax3 = figure.add_axes([2./3, 0, 1./3, 1], frameon=True, xticks=[], yticks=[])
		ax3.imshow(predictionNoBG, interpolation='none')
		figure.savefig('{}/{}'.format(self.predictionsDir, imageName), dpi=dpi)
		plt.close(figure)	

if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("truthDir", help="Directory to load DRIVE test images from. Images must follow DRIVE naming convention.")
	parser.add_argument("scoreFile", help="The test scores numpy file - a dictionary with keys as DRIVE filenames (e.g. 01_test.png) and values as the respective ndarray output from the Caffe model. and plotting/rendering score images along with ground truths to files")
	parser.add_argument("-o", "--outputDir", help="Directory to save images to.", default="testRenderings")
	args = parser.parse_args()
	
	renderer = TestRenderer(args.truthDir, args.scoreFile, args.outputDir)
	renderer.render()