import numpy as np
from ResultsGenerator import ResultsGenerator, DataTransformer
import DRIVE_common as DRIVE
import IO

class DataTransformer2(DataTransformer):
	# input: truthData and scoreData shape: (C, H, W)
	# output: move classes/channels to first dimension and flatten all higher dimensions
	def transformScoresPerClass(self, numClasses, scoreData):
		return scoreData.reshape((numClasses, -1))
		
	def transformTruthsPerClass(self, numClasses, truthData):
		overlap = truthData[3]
		artery = np.maximum(truthData[1], overlap)
		vein = np.maximum(truthData[2], overlap)
		return np.stack([artery, vein], axis=0).reshape((numClasses, -1))
		
def run(scoreFile, truthDir, plotImageFile):
	scoreDict = np.load(scoreFile)
	labelLoader = DRIVE.LabelDataLoaderImagesDRIVE(truthDir, scoreDict.files)
	mm = ResultsGenerator(labelLoader, DataTransformer2(), scoreDict=scoreDict, 
		numClasses=2, classNames=DRIVE.classes[1:3], classColors=DRIVE.classColors[1:3],
		plotImageFile=plotImageFile)		
	mm.plotPrecisionRecall()

if __name__ == "__main__":
	# NOTE: hardcoded script params
	modelName = '160_iter_20000'
	def setupAndRun(dataSet):
		arteryScoreFile = 'artery_{}_{}Scores.npz'.format(modelName, dataSet)
		veinScoreFile = 'vein_{}_{}Scores.npz'.format(modelName, dataSet)	
		avScoreFile = 'av_{}_{}Scores.npz'.format(modelName, dataSet)	
		IO.loadCombineAVScores(arteryScoreFile, veinScoreFile, avScoreFile)
		plotFile = '{}PrecRecall_160.png'.format(dataSet)
		truthDir = '../../../Images/DRIVE/{}/av'.format(dataSet)
		run(avScoreFile, truthDir, plotFile)
	setupAndRun('train')
	setupAndRun('test')