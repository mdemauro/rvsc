import caffe

import os
import numpy as np
#import cv2
from PIL import Image

class InputLayerDRIVE(caffe.Layer):
	"""
	Load (input image, label image) pairs for DRIVE
	one-at-a-time while reshaping the net to preserve dimensions.

	Use this to feed data to a fully convolutional network.
	"""
	def setup(self, bottom, top):
		"""
		Setup data layer according to parameters:

		- DRIVE_dir: path to DRIVE roots directory
		- mean: tuple of mean values to subtract

		example

		params = dict(DRIVE_dir="/path/to/DRIVE",
			mean=(171.0773, 98.4333, 58.8811))
		"""
		# config
		self.params = eval(self.param_str)
		self.DRIVE_dir = self.params['DRIVE_dir']
		self.mean = np.array(self.params['mean'])

		# two tops: data and label
		if len(top) != 2:
			raise Exception("Need to define two tops: data and label.")
		# data layers have no bottoms
		if len(bottom) != 0:
			raise Exception("Do not define a bottom.")

		self.idx = 0

	def reshape(self, bottom, top):
		# load image + label image pair
		self.loadImage(self.idx)
		self.loadLabel(self.idx)
		# reshape tops to fit (leading 1 is for batch dimension)
		top[0].reshape(1, *self.data.shape)
		top[1].reshape(1, *self.label.shape)

	def forward(self, bottom, top):
		# assign output
		top[0].data[...] = self.data
		top[1].data[...] = self.label

		# pick next input
		self.idx += 1
		if self.idx == 20:
			self.idx = 0

	def backward(self, top, propagate_down, bottom):
		pass

	def loadImage(self, idx):
		"""
		Load input image and preprocess for Caffe:
		- cast to float
		- switch channels RGB -> BGR
		- subtract mean
		- transpose to channel x height x width order
		"""
		filePath = '{}/images/{}_training.png'.format(self.DRIVE_dir, str(21+idx).zfill(2))
		if not os.path.exists(filePath):
			raise Exception('DRIVE Image not found: {}'.format(filePath))
		
		# Loading via OpenCV
		#image = cv2.imread(filePath)
		#image = np.array(image, dtype=np.float32)
		#self.data = image[:,:,::-1] # Only swap BGR to RGB if loading with OpenCV
		
		# Loading via PIL
		image = Image.open(filePath)
		imageData = np.array(image.getdata(), np.float32)
		self.data = imageData.reshape(image.size[1], image.size[0], 3)
		
		# Preprocess
		self.data -= self.mean 
		self.data = self.data.transpose((2,0,1)) 
				
	def loadLabel(self, idx):
		"""
		Load label image as 1 x classes x height x width integer array of label indices.
		The leading singleton dimension is required by the loss.
		"""
		filePath = '{}/av/{}_training.png'.format(self.DRIVE_dir, str(21+idx).zfill(2))
		if not os.path.exists(filePath):
			raise Exception('DRIVE Image not found: {}'.format(filePath))
		
		# Loading via OpenCV
		#image = cv2.imread(filePath)
		#image = np.array(image, dtype=np.uint8)

		# Loading via PIL		
		image = Image.open(filePath)
		imageData = np.array(image.getdata(), np.uint8)
		image = imageData.reshape(image.size[1], image.size[0], 3)
		
		self.prepareLabelFromImage(image)
	
	# Override this method for handling ground truth differently in a specific model
	def prepareLabelFromImage(self, image):
		#r,g,b = cv2.split(image)
		r = image[...,0]
		g = image[...,1]
		b = image[...,2]
		unknown = (np.minimum(b, r)) > 0
		background = ((b+g+r) == 0)
		overlap = (g - unknown) > 0
		artery = (r - unknown) > 0
		vein = (b - unknown) > 0
		
		# [1 x C x H x W]
		self.label = np.stack([background, artery, vein, overlap], 0)