@echo off 

SET RVSC=..\..\..
SET SCRIPTS=%RVSC%\scripts
SET PYTHONPATH=%PYTHONPATH%;%SCRIPTS%;
SET DRIVE=%RVSC%\Images\DRIVE
SET MODEL=iter_20000

REM Run Caffe: Generate Test Scores
REM python %SCRIPTS%\ModelTester.py -i %DRIVE%\train\images -f %DRIVE%\train\fileNames.txt -w _%MODEL%.caffemodel -o %MODEL%_trainScores.npz
REM python %SCRIPTS%\ModelTester.py -i %DRIVE%\test\images -f %DRIVE%\test\fileNames.txt -w _%MODEL%.caffemodel -o %MODEL%_testScores.npz
REM python combineScores.py %MODEL%_testScores.npz %MODEL%_testScores_2.npz
REM python combineScores.py %MODEL%_trainScores.npz %MODEL%_trainScores_2.npz

REM Test 1.1: 4 classes (overlap separate from vessels)
REM python %SCRIPTS%\plotPrecisionRecall.py %DRIVE%\train\av %MODEL%_trainScores.npz -o trainPrecRecall_1_1.pdf
REM python %SCRIPTS%\plotPrecisionRecall.py %DRIVE%\test\av %MODEL%_testScores.npz -o testPrecRecall_1_1.pdf
REM python %SCRIPTS%\renderTestOutput.py %DRIVE%\train\av %MODEL%_trainScores.npz -o trainRenderings_1
REM python %SCRIPTS%\renderTestOutput.py %DRIVE%\test\av %MODEL%_testScores.npz -o testRenderings_1

REM Test 1.2, Vessel-Only: (Vessel+Overlap) scores compared against (Vessel+Overlap) truth (no background)
REM python %SCRIPTS%\plotPrecisionRecall.py %DRIVE%\train\av %MODEL%_trainScores_2.npz -o trainPrecRecall_1_2.pdf -v
REM python %SCRIPTS%\plotPrecisionRecall.py %DRIVE%\test\av %MODEL%_testScores_2.npz -o testPrecRecall_1_2.pdf -v
REM python %SCRIPTS%\renderTestOutput.py %DRIVE%\train\av %MODEL%_trainScores_2.npz -o trainRenderings_2
REM python %SCRIPTS%\renderTestOutput.py %DRIVE%\test\av %MODEL%_testScores_2.npz -o testRenderings_2

python generateResults.py