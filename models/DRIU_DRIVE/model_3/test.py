import numpy as np
from ModelTester import ModelTester
from ResultsGenerator import ResultsGenerator, DataTransformer
import DRIVE_common as DRIVE
import IO

def test(inScoreFile, model, weights, outScoreFile):
	tester = ModelTester()
	tester.scoreBlobName = 'sigmoid-fuse'
	tester.testScoresFile = inScoreFile # for inputs to model 3 (this might be confusing..)
	tester.model = model
	tester.weights = weights
	tester.outputScoreFile = outScoreFile
	tester.test()

class DataTransformer3(DataTransformer):
	# input: truthData and scoreData shape: (C, H, W)
	# output: move classes/channels to first dimension and flatten all higher dimensions
	def transformScoresPerClass(self, numClasses, scoreData):
		return scoreData.reshape((numClasses, -1))
		
	def transformTruthsPerClass(self, numClasses, truthData):
		overlap = truthData[3]
		artery = np.maximum(truthData[1], overlap)
		vein = np.maximum(truthData[2], overlap)
		return np.stack([artery, vein], axis=0).reshape((numClasses, -1))
		
def run(scoreFile, truthDir, plotImageFile):
	scoreDict = np.load(scoreFile)
	labelLoader = DRIVE.LabelDataLoaderImagesDRIVE(truthDir, scoreDict.files)
	mm = ResultsGenerator(labelLoader, DataTransformer3(), scoreDict=scoreDict, 
		numClasses=2, classNames=DRIVE.classes[1:3], classColors=DRIVE.classColors[1:3],
		plotImageFile=plotImageFile)		
	mm.plotPrecisionRecall()

if __name__ == "__main__":
	
	# NOTE: hardcoded script params
	# Model 3 is trained/tested using Model 1 output scores as inputs.
	modelName = 'iter_20000'
	modelFile = 'train.prototxt'
	
	def setupAndRun(dataSet):
		inScoreFile = '..\model_1\iter_20000_{}Scores.npz'.format(dataSet)
		
		# test model 1 input scores against vein model
		veinScoreFile = 'vein_{}_{}Scores.npz'.format(modelName, dataSet)
		veinModelWeights = 'vein/_{}.caffemodel'.format(modelName)
		test(inScoreFile, modelFile, veinModelWeights, veinScoreFile)
		
		# test model 1 input scores against artery model
		arteryScoreFile = 'artery_{}_{}Scores.npz'.format(modelName, dataSet)
		arteryModelWeights = 'artery/_{}.caffemodel'.format(modelName)
		test(inScoreFile, modelFile, arteryModelWeights, arteryScoreFile)
		
		# combine scores
		avScoreFile = 'av_{}_{}Scores.npz'.format(modelName, dataSet)
		IO.loadCombineAVScores(arteryScoreFile, veinScoreFile, avScoreFile)
		
		# generate results
		plotFile = '{}PrecRecall.png'.format(dataSet)
		truthDir = '../../../Images/DRIVE/{}/av'.format(dataSet)
		run(avScoreFile, truthDir, plotFile)

	setupAndRun('train')
	setupAndRun('test')