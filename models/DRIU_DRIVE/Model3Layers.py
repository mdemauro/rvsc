from Model2Layers import InputLayerModel2
import os
import numpy as np

class InputLayerModel3(InputLayerModel2):		
	def loadParams(self):
		self.params = eval(self.param_str)
		
		self.scoreFile = self.params['scoreFile']
		assert os.path.exists(self.scoreFile), 'Score file not found: {}'.format(filePath)
		self.scoresDict = np.load(self.scoreFile)
		
		self.imageNamesFile = self.params['imageNamesFile']
		assert os.path.isfile(self.imageNamesFile),	"imageNamesFile doesn't exist: {}".format(self.imageNamesFile)
		
		self.labelImageDir = self.params['labelImageDir']
		assert os.path.isdir(self.labelImageDir), "labelImageDir doesn't exist: {}".format(self.labelImageDir)
		
		self.mean = np.array(self.params['mean'])
		
	def checkImages(self):
		# check that all image files exists
		for imageName in self.imageNames:
			assert imageName in self.scoresDict.files, "Input score doesn't exist for image: {}".format(imageName)
			labelFilePath = os.path.join(self.labelImageDir, imageName)
			assert os.path.isfile(labelFilePath), "Label image file doesn't exist: {}".format(labelFilePath)			

	def loadInput(self, idx):
		self.data = self.scoresDict[self.imageNames[idx]]