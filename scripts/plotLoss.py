import numpy as np
from matplotlib import pyplot as plt

w = [1000, 1]
totalIters = 20000

def plotLossPerImage(losses, plotIters):
	x = range(totalIters/20)
	plt.figure(figsize=(16,9))
	for i in range(20):
		plt.subplot(5, 4, i+1)
		totalLoss = np.zeros((totalIters/20))
		for j in range(losses.shape[0]):
			loss = losses[j,i::20]*w[j]
			totalLoss += loss
			plt.plot(x, loss, lw=2)
		plt.plot(x, totalLoss, lw=2)
		plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
		plt.tick_params(axis='y', which='both', left='off', right='off', labelleft='off')
		plt.xlim([0, plotIters/20])
		plt.ylim([0, 1000000])
		plt.title('{:02d}_training.png'.format(i+1))
		
def plotLossCombined(losses, plotIters):
	x = range(totalIters)
	plt.figure(figsize=(8,8))
	for j in range(losses.shape[0]):
		plt.plot(x, losses[j]*w[j], lw=2)
	plt.plot(x, np.rollaxis(losses, 1, 0).sum(0), lw=2)
	#plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='off')
	#plt.tick_params(axis='y', which='both', left='off', right='off', labelleft='off')
	plt.xlim([0, plotIters])
	plt.ylim([0, 2000000])

if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("-c")
	parser.add_argument("-n", required=True)
	parser.add_argument("-m", required=True)
	args = parser.parse_args()
	n = int(args.n)
	
	losses = np.load('DRIU_DRIVE/model_{}/training_loss.npz'.format(args.m), mmap_mode='r')['loss']
	if args.c:
		plotLossCombined(losses, n)	
	else:
		plotLossPerImage(losses, n)
	plt.tight_layout()
	plt.show()