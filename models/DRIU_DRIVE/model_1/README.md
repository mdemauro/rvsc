Model 1

This is an extension of the DRIU base model from a binary classification to 4 classes - background, artery, vein, overlap

Weights are transplanted from the base model layer 'new-score-weighting' to 'new-score-weighting_av' for only artery and vein classes: indices (1,2)

