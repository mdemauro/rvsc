import os
import numpy as np
import caffe
from PIL import Image

class InputLayerModel3(caffe.Layer):	

	def setup(self, bottom, top):
		# two tops: data and label
		if len(top) != 2:
			raise Exception("Need to define two tops: data and label.")
		# data layers have no bottoms
		if len(bottom) != 0:
			raise Exception("Do not define a bottom.")

		# NOTE: make sure run script copies the appropriate:
		# artery-only/vein-only label images
		# filenames file
		# model_1 score file (expected data shape: (C, H, W))
		self.labelImageDir = 'labels'
		self.loadImageNames('filenames.txt')
		self.preProcessInputData('inputData.npz')		
		self.idx = 0

	def loadImageNames(self, imageNamesFile):
		# load image file names
		self.imageNames = []
		with open(imageNamesFile) as f:
			self.imageNames = [i.strip() for i in f.readlines()]
		assert len(self.imageNames) > 0, "imageNamesFile {} is empty".format(imageNamesFile)
		self.checkImages()
	
	def checkImages(self):
		# check that all image files exists
		for imageName in self.imageNames:
			labelFilePath = os.path.join(self.labelImageDir, imageName)
			assert os.path.isfile(labelFilePath), "Label image file doesn't exist: {}".format(labelFilePath)
	
	def preProcessInputData(self, inputDataFile):
		# loads input scores and performs mean subtraction
		inputDataDict = np.load(inputDataFile)
		images = [inputDataDict[i].transpose(1,2,0) for i in self.imageNames]
		imgVol = np.stack(images, 0)
		mean = np.mean(imgVol, (0,1,2))
		self.inputData = [(i-mean).transpose(2,0,1) for i in images]

	def reshape(self, bottom, top):
		# load image + label image pair
		self.data = self.inputData[self.idx]
		self.loadLabel(self.idx)
		# reshape tops to fit (leading 1 is for batch dimension)
		top[0].reshape(1, *self.data.shape)
		top[1].reshape(1, *self.label.shape)

	def openImageFile(self, filePath):	
		image = Image.open(filePath)
		imageData = np.array(image.getdata(), np.float32)
		numChannels = len(image.getbands())
		return imageData.reshape(image.size[1], image.size[0], numChannels)	
				
	def loadLabel(self, idx):
		labelFilePath = os.path.join(self.labelImageDir, self.imageNames[idx])	
		labelImage = self.openImageFile(labelFilePath) 		
		self.label = labelImage.transpose((2,0,1))
		
	def forward(self, bottom, top):
		# assign output
		top[0].data[...] = self.data[np.newaxis,...]
		top[1].data[...] = self.label[np.newaxis,...]

		# pick next input
		self.idx += 1
		if self.idx >= len(self.imageNames):
			self.idx = 0

	def backward(self, top, propagate_down, bottom):
		pass
		