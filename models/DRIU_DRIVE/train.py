#!/usr/bin/python

import caffe
import numpy as np
import os
import cv2

def train(args):	
	if(args.gpuMode):
		caffe.set_device(0)
		caffe.set_mode_gpu()
		
	# Loads
	solver = caffe.SGDSolver(args.solver)
	iter = 0
	# TODO expose solver params in Caffe API
	#maxIters = solver.param.max_iter
	#maxIters = 20000
	maxIters = args.maxIters
	
	if args.weightsOrSnapshotFile:
		if args.weightsOrSnapshotFile.endswith('solverstate'):
			solver.restore(args.weightsOrSnapshotFile)
			iter = solver.iter	
		elif args.weightsOrSnapshotFile.endswith('caffemodel'):
			solver.net.copy_from(args.weightsOrSnapshotFile)	
		
	if(args.saveLossFile):
		try:
			output = np.load(args.saveLossFile)
			loss = output['loss']
			loss.resize((len(args.lossBlobNames), maxIters))
			output.close()
		except IOError:
			print "Existing loss file ({}) not found, creating new loss file starting from iter: {}.".format(args.saveLossFile, iter)
			loss = np.zeros((len(args.lossBlobNames), maxIters), dtype=np.float32)
	
	# Go! Go! Go!
	for i in range(iter, maxIters):
		solver.step(1)
		
		if(args.saveScores):
			scores = solver.net.blobs[args.scoreBlobName].data[0]
			cv2.imwrite('{}/iter_{}_background.png'.format(args.saveScores, i), np.array(scores[0]*255, dtype=np.uint8))
			cv2.imwrite('{}/iter_{}_artery.png'.format(args.saveScores, i), np.array(scores[1]*255, dtype=np.uint8))
			cv2.imwrite('{}/iter_{}_vein.png'.format(args.saveScores, i), np.array(scores[2]*255, dtype=np.uint8))
			cv2.imwrite('{}/iter_{}_overlap.png'.format(args.saveScores, i), np.array(scores[3]*255, dtype=np.uint8))		
			
		if(args.savePredictions):
			out = scores.argmax(axis=0)
			background = out == 0
			artery = out == 1
			vein = out == 2
			overlap = out == 3
			out = np.stack([artery, overlap, vein],2) * 255
			cv2.imwrite('{}/iter_{}.png'.format(args.savePredictions, i), out)
		
		if(args.saveLossFile):
			for b in range(len(args.lossBlobNames)):
				loss[b,i] = solver.net.blobs[args.lossBlobNames[b]].data.copy()
			np.savez(args.saveLossFile, loss=loss)
		
if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("-m", "--model", help="Model training proto definition file.", default="training.prototxt")
	parser.add_argument("-s", "--solver", help="Solver proto definition file.", default="solver.prototxt")
	#group = parser.add_mutually_exclusive_group(required=True)
	#group.add_argument("-n", "--snapshot", help="Solver snapshot to restore from.")
	parser.add_argument("-f", "--weightsOrSnapshotFile", help="Initial model weights file (.caffemodel) to copy from when starting, or the snapshot file (.solverstate) to restore from and continue.")
	parser.add_argument("-g", "--gpuMode", help="GPU", action="store_true")
	parser.add_argument("-p", "--savePredictions", help="Save prediction images to file", action="store_const", const="output/predictions")
	parser.add_argument("-c", "--saveScores", help="Save score images to file", action="store_const", const="output/scoresImages")
	parser.add_argument("-l", "--saveLossFile", help="File to save loss output to.", action="store_const", const="output/loss.npz")	
	parser.add_argument("--scoreBlobName", help="Name of top blob to pull score from.")
	parser.add_argument("--lossBlobNames", nargs='*', help="Name of top blob(s) to pull loss(es) from.")
	parser.add_argument("--maxIters", type=int, required=True, help="Maximum iterations that solver will train to. This should match the value in solver.prototxt. In the future, this value should be automatically pulled from solver.prototxt - solver params are not currently exposed in Caffe.")
	
	args = parser.parse_args()
	
	if((args.savePredictions or args.saveScores) and not args.scoreBlobName):
		raise RuntimeError("When saving predictions or scores, you must provide the name of your model blob to pull scores from. Set this using --scoreBlobName")
	if(args.saveLossFile and not args.lossBlobNames):
		raise RuntimeError("When saving loss, you must provide the name(s) of your model blob(s) to pull loss(es) from. Set this using --lossBlobNames.")
			
	# Check directories and create if necessary
	def checkCreateDir(dir):
		if(dir and not os.path.exists(dir)):
			os.makedirs(dir)
	checkCreateDir(args.saveScores)
	checkCreateDir(args.savePredictions)
	
	train(args)