import numpy as np
from ResultsGenerator import ResultsGenerator, DataTransformer
import DRIVE_common as DRIVE

class DataTransformer1(DataTransformer):
	combineOverlap = False
	ignoreBackground = False
	
	def transformScoresPerClass(self, numClasses, scoreData):
		return self.transform(numClasses, scoreData)
		
	def transformTruthsPerClass(self, numClasses, truthData):
		return self.transform(numClasses, truthData)
		
	# input: truthData and scoreData shape: (C, H, W)
	# output: move classes/channels to first dimension and flatten all higher dimensions
	def transform(self, numClasses, data):		
		if self.combineOverlap:
			overlap = data[3]
			artery = np.maximum(data[1], overlap)
			vein = np.maximum(data[2], overlap)
			data = np.stack([artery, vein], axis=0)
		elif self.ignoreBackground:
			data = data[1:4]
		return data.reshape((numClasses, -1))
		
def run(modelName, dataSet):
	scoreFile =  '{}_{}Scores.npz'.format(modelName, dataSet)
	truthDir =  '../../../Images/DRIVE/{}/av'.format(dataSet)
	scoreDict = np.load(scoreFile)
	labelLoader = DRIVE.LabelDataLoaderImagesDRIVE(truthDir, scoreDict.files)
	dataTransformer = DataTransformer1()
	dataTransformer.ignoreBackground = True
	mm = ResultsGenerator(labelLoader, dataTransformer, scoreDict=scoreDict, 
		numClasses=3, classNames=DRIVE.classes[1:4], classColors=DRIVE.classColors[1:4])			
	mm.plotPrecisionRecall(plotImageFile='{}PrecRecall.png'.format(dataSet))
	mm.plotConfusionMatrix(plotImageFile='{}ConfusionMatrix.png'.format(dataSet))
		
if __name__ == "__main__":	
	modelName = 'iter_20000'
	run(modelName, 'train')
	run(modelName, 'test')