@echo off 

SET RVSC=..\..\..
SET SCRIPTS=%RVSC%\scripts
SET PYTHONPATH=%PYTHONPATH%;%SCRIPTS%;
SET DRIVE=%RVSC%\Images\DRIVE
SET MODEL=iter_160_20000

REM Run Caffe: Generate Test Scores
python %SCRIPTS%\ModelTester.py -i %DRIVE%\train\images -f %DRIVE%\train\fileNames.txt -m ..\model_2a\train_a.prototxt -w ..\model_2a\artery_%MODEL%.caffemodel -o artery_%MODEL%_trainScores.npz
python %SCRIPTS%\ModelTester.py -i %DRIVE%\test\images -f %DRIVE%\test\fileNames.txt -m ..\model_2a\train_a.prototxt -w ..\model_2a\artery_%MODEL%.caffemodel -o artery_%MODEL%_testScores.npz
python %SCRIPTS%\ModelTester.py -i %DRIVE%\train\images -f %DRIVE%\train\fileNames.txt -m train_v.prototxt -w vein_%MODEL%.caffemodel -o vein_%MODEL%_trainScores.npz
python %SCRIPTS%\ModelTester.py -i %DRIVE%\test\images -f %DRIVE%\test\fileNames.txt -m train_v.prototxt -w vein_%MODEL%.caffemodel -o vein_%MODEL%_testScores.npz

REM Generate Results (TODO parameterize generateResults.py - currently, hardcodes need manual change)
REM python generateResults.py