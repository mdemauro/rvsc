#!/bin/bash

set -x

# Call the common training script (should be copied into current directory by SLURM script) 
# with model-specific parameters
#python train.py -f $1 -g -l --scoreBlobName upscore-fuse --lossBlobNames sigmoid-fuse --maxIters 30000 > output/train.log
$CAFFE_ROOT/caffe train -solver=solver.prototxt -weights=train_start.caffemodel
