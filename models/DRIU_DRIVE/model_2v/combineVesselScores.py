import os
import numpy as np

'''
This script is for loading two separate score files (one for vein, one for artery)
and combining them into a single score file for calculating metrics and rendering
'''
		
def parseArgs():
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("arteryScoreFile")
	parser.add_argument("veinScoreFile")
	parser.add_argument("outputScoreFile")
	args = parser.parse_args()
			
	return args
	
if __name__ == "__main__":
	args = parseArgs()
	
	a = np.load(args.arteryScoreFile)
	v = np.load(args.veinScoreFile)
	if len(a.files) != len(v.files):
		raise Error("Score files do not have the number of images: len(arteryScoreFile)={}, len(veinScoreFile)={}".format(len(a),len(v)))
		
	outDict = {}
	for k in a.keys():
		scoreA = a[k]
		scoreV = v[k]
		if not k in v.files:
			raise Error("Key not found in veinScoreFile: {}".format(k))
		if(scoreA.shape != scoreV.shape):
			raise Error("Scores shape mismatch: artery={}, vein={}".format(scoreA.shape, scoreV.shape))
		outDict[k] = np.concatenate([scoreA, scoreV])
		
	np.savez(args.outputScoreFile, **outDict)